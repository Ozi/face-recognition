//
//  AlertController.swift
//  Face-Recognition
//
//  Created by Muchamad Fauzi on 29/05/23.
//

import UIKit

class AlertController: UIAlertController {
    
    private lazy var alertWindow: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = ClearViewController()
        window.backgroundColor = UIColor.clear
        window.windowLevel = UIWindow.Level.alert
        return window
    }()
    
    func show(animated flag: Bool = true, completion: (() -> Void)? = nil) {
        guard let rootVC = alertWindow.rootViewController else { return }
        alertWindow.makeKeyAndVisible()
        rootVC.present(self, animated: flag, completion: completion)
    }
}

private class ClearViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if let statusBarManager = view.window?.windowScene?.statusBarManager {
            return statusBarManager.statusBarStyle
        } else {
            return super.preferredStatusBarStyle
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        if let statusBarManager = view.window?.windowScene?.statusBarManager {
            return statusBarManager.isStatusBarHidden
        } else {
            return super.prefersStatusBarHidden
        }
    }
}
