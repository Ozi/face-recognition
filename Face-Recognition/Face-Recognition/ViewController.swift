//
//  ViewController.swift
//  Face-Recognition
//
//  Created by Muchamad Fauzi on 29/05/23.
//

import UIKit
import ARKit
import SnapKit

class ViewController: UIViewController {

    private var sceneView = ARSCNView()
    private let THRESHOLD: CGFloat = 0.7
    private var stepAction = 1
    private var textLabel = UILabel()
    
    private var isSaving = false
    
    var isSupported: Bool {
        return ARFaceTrackingConfiguration.isSupported
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sceneView.delegate = self
        setupView()
        
        if isSupported {
            startFaceDetection()
        } else {
            showAlert()
        }
    }
    
    private func setupView() {
        
        view.backgroundColor = .gray
        
        view.addSubview(sceneView)
        sceneView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().inset(50)
        }
        
        view.addSubview(textLabel)
        textLabel.text = "Hello"
        textLabel.snp.makeConstraints { make in
            make.top.equalTo(sceneView.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
        }
    }

    private func startFaceDetection() {
        let configuration = ARFaceTrackingConfiguration()
        configuration.isLightEstimationEnabled = true
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
    
    private func showAlert() {
        let alert = AlertController(title: "Error", message: "Perangkat Anda tidak mendukung pelacakan wajah", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(action)
        alert.show()
    }

    private func savePhoto() {
        if isSaving { return }
        isSaving = true
        displayShutterAnimation()
        UIImageWriteToSavedPhotosAlbum(sceneView.snapshot(), nil, nil, nil)
        sceneView.session.pause()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.startFaceDetection()
            self?.isSaving = false
        }
    }
    
    private func displayShutterAnimation() {
        let shutterAnimation = CATransition.init()
        shutterAnimation.duration = 0.6
        shutterAnimation.timingFunction = CAMediaTimingFunction.init(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        shutterAnimation.type = CATransitionType(rawValue: "camSwift")
        shutterAnimation.setValue("camSwift", forKey: "camSwift")
        
        let shutterLayer = CALayer.init()
        shutterLayer.bounds = view.bounds
        view.layer.addSublayer(shutterLayer)
        view.layer.add(shutterAnimation, forKey: "camSwift")
    }
    
}

extension ViewController: ARSCNViewDelegate {
    public func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let faceAnchor = anchor as? ARFaceAnchor else { return }
        let blendShapes = faceAnchor.blendShapes
        
        if stepAction == 1 {
            
            DispatchQueue.main.async {
                self.textLabel.text = "Silahkan Senyum"
            }
            
            if let left = blendShapes[.mouthSmileLeft], let right = blendShapes[.mouthSmileRight] {
                let smileParameter = min(max(CGFloat(truncating: left), CGFloat(truncating: right))/THRESHOLD, 1.0)
                DispatchQueue.main.async { [weak self] in
                    if smileParameter == 1 {
                        print("senyum")
                        self?.savePhoto()
                        self?.stepAction = 2
                    }
                }
            }
        }else if stepAction == 2 {
            
            DispatchQueue.main.async {
                self.textLabel.text = "Silahkan gerakkan kepala ke kanan"
            }

            if let lookRight = blendShapes[.eyeLookOutRight] {
                let lookParameter = CGFloat(truncating: lookRight)
                DispatchQueue.main.async { [weak self] in
                    if lookParameter >= self?.THRESHOLD ?? 1 {
                        print("Noleh kekanan")
                        self?.savePhoto()
                        self?.stepAction = 3
                    }
                }
            }
        }else if stepAction == 3 {
            
            DispatchQueue.main.async {
                self.textLabel.text = "Silahkan gerakkan kepala ke kiri"
            }
            
            if let lookLeft = blendShapes[.eyeLookOutLeft] {
                let lookParameter = CGFloat(truncating: lookLeft)
                DispatchQueue.main.async { [weak self] in
                    if lookParameter >= self?.THRESHOLD ?? 1 {
                        print("Noleh kekiri")
                        self?.savePhoto()
                        self?.stepAction = 4
                    }
                }
            }
        }else if stepAction == 4 {
            
            DispatchQueue.main.async {
                self.textLabel.text = "Silahkan berkedip"
            }
            
            if let blinkRight = blendShapes[.eyeBlinkRight], let blinkLeft = blendShapes[.eyeBlinkLeft] {
                let blinkParameter = min(max(CGFloat(truncating: blinkRight), CGFloat(truncating: blinkLeft))/THRESHOLD, 1.0)
                DispatchQueue.main.async { [weak self] in
                    if blinkParameter == 1 {
                        print("kedip")
                        self?.savePhoto()
                        self?.stepAction = 5
                    }
                }
            }
        }else if stepAction == 5 {
            DispatchQueue.main.async {
                self.textLabel.text = "Sudah sesuai. Terima kasih"
            }
        }
    }
    
}



